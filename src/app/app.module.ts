import { CalibrationService } from './../services/calibration.service';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ConfigService } from '../services/config.service';
import { RequestService } from '../services/request.service';
import { HttpModule } from '@angular/http';
import { ColorPipe } from '../pipes/color.pipe';
import { digitalToWeightPipe } from '../pipes/digitalToWeight.pipe';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,


    //Pipes
    ColorPipe,
    digitalToWeightPipe,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ConfigService,
    RequestService,
    CalibrationService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
