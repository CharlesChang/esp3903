
export type DataTable = DataRow[]
export type DataRow = string[]


export type DataTableNumber = DataRowNumber[]
export type DataRowNumber = number[];