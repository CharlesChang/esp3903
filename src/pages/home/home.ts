import { CalibrationService } from './../../services/calibration.service';
import { DataTable } from './../../models/data.model';
import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RequestService } from '../../services/request.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {
  constructor(public navCtrl: NavController, private request: RequestService, public cs: CalibrationService) {

  }
  public totalWeight$ = new BehaviorSubject(0);
  public isConnected = false;
  public dataTable$: BehaviorSubject<DataTable> = new BehaviorSubject([
    [],
    [],
  ]);

  trackByFn(index: number) {
    return index
  }
  ngOnInit(): void {
    let id = setInterval(() => {
      this.request.readData(1).subscribe(
        data => {
          this.dataTable$.next(data);
          this.isConnected = true;
        },
        err => {
          console.log(err);
          this.isConnected = false;
        },
      )
    }, 200)


    //relay weight information
    this.cs.totalWeight$.subscribe(val => this.totalWeight$.next(val));
  }

  public calibrate(dataTable = this.dataTable$.getValue()): void {
    this.cs.recordOffSet(dataTable);
    this.dataTable$.next(this.dataTable$.getValue());
    console.log("calibrated")
  }




}
