import { CalibrationService } from './../services/calibration.service';
import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
  name: "colorPipe"
})
export class ColorPipe implements PipeTransform {
  constructor(private cs: CalibrationService) { }
  transform(voltage: string, row: number, col: number) {
    let volt = parseFloat(voltage);
    let percentage = this.cs.calculatePercentage(volt, row, col)


    return `rgba(255, 0, 0, ${percentage} )`;
  }
}