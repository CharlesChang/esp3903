import { CalibrationService } from './../services/calibration.service';
import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
  name: "diToW"
})
export class digitalToWeightPipe implements PipeTransform {
  constructor(private cs: CalibrationService) { }
  transform(voltage: string, row: number, col: number): string {
    let volt = parseFloat(voltage);

    let weight = (this.cs.calculatePercentage(volt, row, col) * 1000).toPrecision(3);

    this.cs.updateWeight(parseInt(weight), row, col);
    
    return weight;
  }
}