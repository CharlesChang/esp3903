import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { DataTable, DataTableNumber } from './../models/data.model';
import { Injectable } from '@angular/core';

  
@Injectable()
export class CalibrationService {
  public minVoltage = 4.0;
  public maxVoltage = 5.0;
  public voltRange = this.maxVoltage - this.minVoltage;

  public dangerousScore = 3000;
  public eachWeight: DataTableNumber = [[], [], [], [], [], [], [], []];
  public totalWeight$ = new BehaviorSubject(0);

  public calibrated$ = new BehaviorSubject<boolean>(false);
  public offSetTable: DataTable = [[], [], [], [], [], [], [], []];


  public recordOffSet(dataTable: DataTable): void {
    for (let row = 0; row < dataTable.length; row++) {
      for (let col = 0; col < dataTable[row].length; col++) {
        this.offSetTable[row][col] = `${this.maxVoltage - parseFloat(dataTable[row][col])}`;
      }
    }

    this.calibrated$.next(true);
  }


  public calculatePercentage(input: number, col: number, row: number): number {
    let percentage: number;
    if (this.calibrated$.getValue() === true) {
      percentage = 1.00 - (input - this.minVoltage + parseFloat(this.offSetTable[row][col])) / (this.voltRange)
    } else {
      percentage = 1.00 - (input - this.minVoltage) / (this.voltRange)
    }

    return percentage;
  }

  /**
   * update the calculated total weight
   * @param input 
   * @param row 
   * @param col 
   */
  public updateWeight(input: number, row: number, col: number) {
    let total = this.totalWeight$.getValue();
    if (!isNaN(this.eachWeight[row][col])) {
      total -= this.eachWeight[row][col];
    }
    this.eachWeight[row][col] = input;
    total += input;
    this.totalWeight$.next(total);

  }
}