import { DataTable } from './../models/data.model';
import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConfigService } from "./config.service";
import { Observable } from "rxjs/Observable";

@Injectable()
export class RequestService {
  constructor(private http: HttpClient, private config: ConfigService) {

  }


  public readData(row: number) {
    return this.http.get<DataTable>(`${this.config.baseUrl}/api/data`);
  }
}